<?php
/**
 * @copyright	Copyright (C) 2011 Simplify Your Web, Inc. All rights reserved.
 * @license		GNU General Public License version 3 or later; see LICENSE.txt
*/

defined( '_JEXEC' ) or die;

use Joomla\CMS\Form\FormHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;

FormHelper::loadFieldClass('dynamicsingleselect');

class JFormFieldDatasourceSelect extends JFormFieldDynamicSingleSelect
{
	public $type = 'DatasourceSelect';

	protected function getOptions()
	{
	    $options = parent::getOptions();
	    
	    \JLoader::register('SYWK2', JPATH_LIBRARIES.'/syw/k2.php');
	    
	    $options[] = array('k2', Text::_('MOD_TRULYRESPONSIVESLIDER_VALUE_K2ITEMS'), '', '', '', !SYWK2::exists());
	    
	    $imagefolder = '/media/syw_trulyresponsiveslides/images/datasources';
	    
	    foreach ($options as &$option) {
	        
	        $option[3] = Uri::root(true).$imagefolder.'/'.$option[0].'.png';
	    }
	    
	    return $options;
	}
	
	public function setup(SimpleXMLElement $element, $value, $group = null)
	{
	    $return = parent::setup($element, $value, $group);
	    
	    if ($return) {
	        $this->width = 100;
	        $this->height = 100;
	    }
	    
	    return $return;
	}
}
?>